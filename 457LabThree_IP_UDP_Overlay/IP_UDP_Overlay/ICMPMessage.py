'''
Class sets up a model for an icmp message to be sent if a router cant find
a router to send packet

@author: Joshua and James Engelsma
@version: 1.0
'''

from IpHeader import IpHeader
import struct
import binascii
import socket

class ICMPMessage(object):
    def __init__(self, type, code, sourceIP, destinationIP, packet):
        '''
        @param headerFields: A dictionary containing our fields to put into our ip header
        '''
        self.type=type
        self.code=code
        self.checksum = 0
        dataHeader = self.addDataBytes(packet)
        datahex = [elem.encode("hex") for elem in dataHeader]
        print(datahex)
        newIPHeader = self.buildNewIPHeader(sourceIP, destinationIP)
        datahex = [elem.encode("hex") for elem in newIPHeader]
        print(datahex)
        icmpHeader = self.buildICMPHeader() #calc icmp header with checksum = 0
        datahex = [elem.encode("hex") for elem in icmpHeader]
        print(datahex)
        oldIPHeader = self.getOldIPHeader(packet)
        datahex = [elem.encode("hex") for elem in oldIPHeader]
        print(datahex)
        self.checksum = self.icmpCheckSum(icmpHeader + oldIPHeader + dataHeader)
        icmpHeader = self.buildICMPHeader() #recalc icmp header with actual checksum
        datahex = [elem.encode("hex") for elem in icmpHeader]
        print(datahex)
        self.packet = newIPHeader + icmpHeader + oldIPHeader + dataHeader
        
    def buildNewIPHeader(self, srcAddress, destAddress):
        '''
        Method returns bytes for a new ip header
        '''
        ipSource = binascii.hexlify(socket.inet_aton(srcAddress))
        ipDest = binascii.hexlify(socket.inet_aton(destAddress))
        ipheader = IpHeader(ipSource, ipDest, '01')
        ipheader.versionAndIhl = 0x45
        ipheader.ttl = 0x60
        ipheader.totalLength = 56 #newip(20) + icmp(8) + oldip(20) + 8data(8) = 56 bytes
        ipheader.protocol=0x01
        ipheader.refreshHexString('01')
        ipCheckSum = self.ipCheckSum(ipheader.getHexString())
        ipheader.checkSum = ipCheckSum
        ipheader.refreshHexString('01')
        ipformatString = '!BBHHHBBHLL'
        header = struct.pack(ipformatString, ipheader.versionAndIhl, ipheader.tos, ipheader.totalLength, ipheader.fragmentID, ipheader.fragmentOffset,
                           ipheader.ttl, ipheader.protocol, ipheader.checkSum, int(ipheader.sourceAddress,16), int(ipheader.destAddress,16))
        return header
    
    def getOldIPHeader(self, packet):
        '''
        Method returns bytes for old ip header
        '''
        hexVals = [packet[i].encode("hex") for i in range(20)]
        hexValFormatString = '!B'
        oldIPHdr = struct.pack(hexValFormatString, int(hexVals[0], 16)) #take first 20 bytes of packet recieved
        for iter in range(1, 20):
            oldIPHdr += struct.pack(hexValFormatString, int(hexVals[iter], 16))
        return oldIPHdr
        
    def buildICMPHeader(self):
        '''
        Method returns bytes for icmp header
        '''
        icmpformatString = '!BBHHH'
        header = struct.pack(icmpformatString, self.type, self.code, self.checksum, 0, 0)
        return header
        
    def addDataBytes(self, packet):
        '''
        Method returns the first 8 or less bytes from the original message
        '''
        hexVals = [elem.encode("hex") for elem in packet]
        dataBytes = []
        for udpIndex in range(20, 28): #return udp bytes
            dataBytes.append(hexVals[udpIndex])
        hexValFormatString = '!B'
        datagram = struct.pack(hexValFormatString, int(dataBytes[0], 16))
        for iter in range(1, len(dataBytes)):
            datagram += struct.pack(hexValFormatString, int(dataBytes[iter], 16))
        return datagram
                    
        
    def ipCheckSum(self, header):
        '''
        calculates check sum for icmp header
        '''
        hexSections = header.split()
        result = 0
        for piece in hexSections:
            intPiece = int(piece, 16)
            result += intPiece
            binResult = bin(result)
            if len(binResult) == 19:
                result -= 65536
                result += 1
        binResult = bin(result)[2:]
        while(len(binResult) < 16):
            binResult = ('0' + binResult)
        onesComp = ''
        for bit in binResult:
            if bit == '0':
                onesComp += '1'
            if bit == '1':
                onesComp += '0'
        finalByte = int(onesComp, 2)
        return finalByte
    
    def icmpCheckSum(self, data):
        hexVals = [elem.encode("hex") for elem in data]
        hexString = ''
        for i in range(0, len(hexVals)-1, 2):
            hex1 = hexVals[i]
            hex2 = hexVals[i+1]
            twoByte = hex1 + hex2
            hexString += (twoByte + ' ')
        hexString.strip()
        print("hex string: {}".format(hexString))
        return self.ipCheckSum(hexString)
            
                
        
        