This configuration represents a simple dumbbell topology, which will
be explained in lab on the first day of this project. It assumes that
you are running your programs on specific hosts in the datacomm lab,
specifically, those with the last octet of the IP address matching the
number in the file name of the configuration file. You are free to
modify these files to create other topologies, but you should not
change the format.
