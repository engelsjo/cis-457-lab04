'''
Program routes packets on our network

@author: Joshua Engelsma and James Engelsma
@version 1.0
'''

import socket
import os
from ICMPMessage import ICMPMessage
import struct

class Router(object):
    
    def __init__(self, port=3490):
        filename = raw_input("Please enter your configuration file: ")
        filepath = '{}/ConfigurationFiles/{}'.format(os.path.abspath(os.path.dirname(__file__)), filename)
        configFile = open(filepath)
        self.configurations = [line for line in configFile]
        
        #set up our udp socket
        self.port = port
        self.address = '' #IN_ADDR
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind((self.address,self.port))
        
        #listen for packets to route on our udp socket...
        running = True
        while running:
            try:
                data, clientAddress = self.socket.recvfrom(3490)
                print('received packet from {}'.format(clientAddress))
                datahex = [elem.encode("hex") for elem in data]
                print(datahex)
        
                #determine what to do with our data...
                self.destinationIP = self.getPacketDestination(data)
                if len(datahex) < 56: #not icmp error packet...
                    self.message = self.getPacketMessage(data)
                else:
                    self.message = '' #no message needed for icmp error
                self.newTTL = self.decreaseTimeToLive(data, 1)
                self.srcLink = self.getPacketSrc(data)
                #process packet
                self.processPacket(self.destinationIP, self.srcLink, self.message, self.newTTL, data, clientAddress)
            except KeyboardInterrupt:
                running = False
                break
           
    def getPacketDestination(self, packet):
        '''
        @param packet: a list of bytes
        @return: the destination ip address in the packet we received
        Method takes in the packet we got, and returns the intended destination ip address.
        '''
        bytesList = [elem.encode("hex") for elem in packet]
        destinationHex = [bytesList[16], bytesList[17], bytesList[18], bytesList[19]]
        ipAddress = ''
        for byte in destinationHex:
            decimal = int(byte, 16)
            ipAddress += (str(decimal) + '.')
        ipAddress = ipAddress[:-1]
        return ipAddress
    
    def getPacketSrc(self, packet):
        bytesList = [elem.encode("hex") for elem in packet]
        srcHex = [bytesList[12], bytesList[13], bytesList[14], bytesList[15]]
        ipAddress = ''
        for byte in srcHex:
            decimal = int(byte, 16)
            ipAddress += (str(decimal) + '.')
        ipAddress = ipAddress[:-1]
        return ipAddress
    
    def getPacketMessage(self, packet):
        '''
        Method returns the message of the packet
        '''
        bytesList = [elem.encode("hex") for elem in packet]
        message = ''
        i = 28
        while i < len(bytesList):
            currentLetter = str(unichr(int(bytesList[i],16)))
            message += currentLetter
            i += 1
        return message
    
    def decreaseTimeToLive(self, packet, decValue):
        '''
        Method decreases the time to live for a packet on the network
        '''
        bytesList = [elem.encode("hex") for elem in packet]
        currentTTL = int(bytesList[8], 16)
        if currentTTL > 0:
            currentTTL -= decValue
        return currentTTL
    
    def ipCheckSum(self, header):
        '''
        calculates check sum for ip header
        '''
        hexSections = header.split()
        result = 0
        for piece in hexSections:
            intPiece = int(piece, 16)
            result += intPiece
            binResult = bin(result)
            if len(binResult) == 19:
                result -= 65536
                result += 1
        binResult = bin(result)[2:]
        while(len(binResult) < 16):
            binResult = ('0' + binResult)
        onesComp = ''
        for bit in binResult:
            if bit == '0':
                onesComp += '1'
            if bit == '1':
                onesComp += '0'
        finalByte = int(onesComp, 2)
        return finalByte
    
    def getHexStringFromPacket(self, packet):
        hexVals = [elem.encode("hex") for elem in packet]
        hexString = ''

        for i in range(0,19,2):
            hex1 = hexVals[i]
            hex2 = hexVals[i+1]
            twoByte = hex1 + hex2
            hexString += (twoByte + ' ')
        return hexString.strip()
            
    def processPacket(self, destinationIP, srcIP,message, newTTL, packet, clientAddress):
        routerIPs = []
        for lineIndex in range(len(self.configurations)):
            if 'address' in self.configurations[lineIndex]:
                routerIPs.append(self.configurations[lineIndex].split(' ')[1].strip())
        if destinationIP in routerIPs and newTTL != 0:
            print("This packet was intended for you mr.router. The message is {}".format(message))
            return
        sendAddress = ''
        for line in self.configurations:
            if 'prefix' in line and '24' in line:
                ipSection = line.split(' ')[1]
                ipPiece = ipSection.split('/')[0]
                ipParts = ipPiece.split('.')
                ipParts.pop()
                destParts = destinationIP.split('.')
                destParts.pop()
                if destParts == ipParts:
                    sendAddress = line.split(' ')[2].strip()
                    break
            elif 'prefix' in line and '16' in line:
                ipSection = line.split(' ')[1]
                ipPiece = ipSection.split('/')[0]
                ipParts = ipPiece.split('.')
                ipParts.pop()
                ipParts.pop()
                destParts = destinationIP.split('.')
                destParts.pop()
                destParts.pop()
                if destParts == ipParts:
                    sendAddress = line.split(' ')[2].strip()
                    break
            elif 'prefix' in line and '8' in line:
                ipSection = line.split(' ')[1]
                ipPiece = ipSection.split('/')[0]
                ipParts = ipPiece.split('.')
                ipParts.pop()
                ipParts.pop()
                ipParts.pop()
                destParts = destinationIP.split('.')
                destParts.pop()
                destParts.pop()
                destParts.pop()
                if destParts == ipParts:
                    sendAddress = line.split(' ')[2].strip()
                    break
        if sendAddress != '' and newTTL != 0:
            self.routeUpdatedPacket(newTTL, packet, sendAddress)
            return
        elif newTTL == 0:
            print("This packets time to live has run out")
            type = 11
            code = 0
            sourceIP = routerIPs[0]
            destinationIP = srcIP
            icmpMessage = ICMPMessage(type, code, sourceIP, destinationIP, packet).packet
            newTTL = self.decreaseTimeToLive(icmpMessage, 1)
            self.routeUpdatedPacket(newTTL, icmpMessage, clientAddress[0])
            return
        else:
            type = 3
            code = 7
            sourceIP = routerIPs[0]
            destinationIP = srcIP
            icmpMessage = ICMPMessage(type, code, sourceIP, destinationIP, packet).packet
            newTTL = self.decreaseTimeToLive(icmpMessage, 1)
            self.routeUpdatedPacket(newTTL,icmpMessage, clientAddress[0])
            return

    def routeUpdatedPacket(self, newTTL, packet, sendAddress):
        '''
        @param newTTL: The newest time to live for the packet
        @param packet: the packet you received.
        This method will take our packet and route to the appropriate client/router
        '''
        print("routing your message")
        hexString = self.getHexStringFromPacket(packet)
        hexString.strip()
        checksum = self.ipCheckSum(hexString)
        if checksum == 0: #got correct data
            packetAsList = list(packet)
            packetAsList[8] = struct.pack('!B', newTTL)
            packet = ''.join(packetAsList)
            hexString = self.getHexStringFromPacket(packet)
            hexList = hexString.split(' ')
            hexList[5] = '0000'
            hexString = ''
            for val in hexList:
                hexString += val + ' '
            hexString = hexString.strip()
            checksum = self.ipCheckSum(hexString)
            checkHexString = str(hex(checksum))[2:]
            if len(checkHexString) < 3:
                byte1 = 0
                byte2 = int(checkHexString, 16)
            elif len(checkHexString) == 3:
                byte1 = int(checkHexString[:1], 16)
                byte2 = int(checkHexString[-2:], 16)
            elif len(checkHexString) == 4:
                byte1 = int(checkHexString[:2], 16)
                byte2 = int(checkHexString[-2:], 16)
            packetAsList = list(packet)
            packetAsList[10] = struct.pack('!B', byte1)
            packetAsList[11] = struct.pack('!B', byte2)
            packet = ''.join(packetAsList)
            
            self.socket.sendto(packet, (sendAddress, 3490))
        
    def sendICMPMessageToSender(self, packet, clientAddress):
        '''
        @param packet: send icmp message back to sender if not route is found
        '''            
        print("sending error message") 
        self.socket.sendto(packet, clientAddress)  
                   
if __name__ == '__main__':
    Router()       