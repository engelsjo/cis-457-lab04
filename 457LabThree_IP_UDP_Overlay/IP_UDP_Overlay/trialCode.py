'''
python module which allows us to quickly guess and test
'''

header = '4500 003c 1c46 4000 4006 0000 ac10 0a63 ac10 0a0c'
hexSections = header.split()
result = 0
for piece in hexSections:
    intPiece = int(piece, 16)
    result += intPiece
    binResult = bin(result)
    if len(binResult) == 19:
        result -= 65536
        result += 1
binResult = bin(result)[2:]
while(len(binResult) < 16):
    binResult = ('0' + binResult)
onesComp = ''
for bit in binResult:
    if bit == '0':
        onesComp += '1'
    if bit == '1':
        onesComp += '0'
print(onesComp)
finalByte = hex(int(onesComp, 2))
return finalByte