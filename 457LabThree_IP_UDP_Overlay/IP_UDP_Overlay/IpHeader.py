'''
Builds an IP Header

@author: James Engelsma and Joshua Engelsma
@version: 1.0
'''

class IpHeader(object):
    
    def __init__(self, sourceAddress, destAddress, protocol='11'):
        '''
        @param headerFields: A dictionary containing our fields to put into our ip header
        '''
        self.versionAndIhl=0x45
        self.tos=0x00
        self.totalLength= 28 #needs to be calculated
        self.fragmentID=0x0000 #not needed
        self.fragmentOffset=0x0000 #not needed
        self.ttl = 0x60 #seconds
        self.protocol=0x11 
        self.checkSum=0 #needs to be calculated
        self.sourceAddress=sourceAddress
        self.destAddress=destAddress
        if protocol != '11':
            self.hexString = '4500 {} 0000 0000 60{} {} {} {} {} {}'.format(str(hex(self.totalLength))[2:],protocol,
                                                                str(self.checkSum)[2:], str(self.sourceAddress)[:-4],
                                                                str(self.sourceAddress)[4:], str(self.destAddress)[:-4],
                                                                str(self.destAddress)[4:])
        else:
            self.hexString = '4500 {} 0000 0000 6011 {} {} {} {} {}'.format(str(hex(self.totalLength))[2:],
                                                                str(self.checkSum)[2:], str(self.sourceAddress)[:-4],
                                                                str(self.sourceAddress)[4:], str(self.destAddress)[:-4],
                                                                str(self.destAddress)[4:]) 
        
    def getHexString(self):
        return self.hexString
    
    def refreshHexString(self, protocol='11'):
        if protocol != '11':
            self.hexString = '4500 {} 0000 0000 60{} {} {} {} {} {}'.format(str(hex(self.totalLength))[2:],protocol,
                                                                str(self.checkSum)[2:], str(self.sourceAddress)[:-4],
                                                                str(self.sourceAddress)[4:], str(self.destAddress)[:-4],
                                                                str(self.destAddress)[4:])
        else:
            self.hexString = '4500 {} 0000 0000 6011 {} {} {} {} {}'.format(str(hex(self.totalLength))[2:],
                                                                str(self.checkSum)[2:], str(self.sourceAddress)[:-4],
                                                                str(self.sourceAddress)[4:], str(self.destAddress)[:-4],
                                                                str(self.destAddress)[4:])
        
        
        