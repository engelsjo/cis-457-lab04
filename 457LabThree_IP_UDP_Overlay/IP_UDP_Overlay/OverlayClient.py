'''
Client program which takes a message and ip address
builds a packet and sends through a UDP packet.

@author: James and Joshua Engelsma
@version: 1.0
'''

import socket
import os
import struct
from AppHeader import AppHeader
from IpHeader import IpHeader
from TransHeader import TransHeader
import binascii

class Client(object):
    
    def __init__(self, port=3490):
        configFile = raw_input("Enter configuration file: ")
        
        self.address = ''
        ipAddress = ''
        self.port = port
        filePath = '{}/ConfigurationFiles/{}'.format(os.path.abspath(os.path.dirname(__file__)), configFile)
        hostFile = open(filePath)
        for line in hostFile:
            lineElements = line.strip().split()
            if lineElements[0] == 'address':
                self.address = lineElements[1]
            if lineElements[0] == 'prefix':
                ipAddress = lineElements[2]
                break
            
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(('',self.port))
        
        running = True
        while running:
            try:
                command = raw_input("Send or Rec? ")
                if command == 'send':
                    message = raw_input("Enter your message to be sent: ")
                    destIP = raw_input("Enter the destination ip to send to ")
        
                    print("you are sending '{}' to {}".format(message, destIP))
                    #set up ip header
                    ipSource = binascii.hexlify(socket.inet_aton(self.address))
                    ipDest = binascii.hexlify(socket.inet_aton(destIP))
                    ipheader = IpHeader(ipSource, ipDest)
                    ipheader.totalLength = self.calcIPLength(message)
                    ipheader.refreshHexString()
                    ipCheckSum = self.ipCheckSum(ipheader.getHexString())
                    ipheader.checkSum = ipCheckSum
                    ipheader.refreshHexString()
        
                    #set up udp header
                    udpheader = TransHeader()
                    udpheader.length = self.calcUDPLength(message)
                    udpheader.refreshHexString()
                    udpheader.checkSum = self.udpCheckSum(ipheader.getHexString(), udpheader.hexString, message)
                    udpheader.refreshHexString()
        
                    #set up application header
                    appheader = AppHeader()
                    appheader.message = message
        
                    packet = self.packHeadersIntoBitString(ipheader, udpheader, appheader)
                    hexRequest = [elem.encode("hex") for elem in packet]
                    print(hexRequest)
                    self.socket.sendto(packet, (ipAddress,3490))
                    
                data, address = self.socket.recvfrom(3490)
                print("received from {}".format(address))
                hexString = self.getHexStringFromPacket(data)
                checksum = self.ipCheckSum(hexString)
                print("Your checksum was {}".format(checksum))
                data = [elem.encode("hex") for elem in data]
                msg = self.processPacket(data)
                print(msg)
            except KeyboardInterrupt:
                running = False
                break
            
    def processPacket(self, bytesList):
        messageToPrint = ''
        packetLength = int(bytesList[2], 16) + int(bytesList[3], 16)
        if packetLength == 56:
            messageToPrint = 'you received an icmp error on a packet you sent'
            print bytesList
        else: #packet was routed to you
            msgFromPkt = ''
            for msgIndex in range(28, len(bytesList)):
                msgFromPkt += str(unichr(int(bytesList[msgIndex],16)))
            srcHex = [bytesList[12], bytesList[13], bytesList[14], bytesList[15]]
            ipAddress = ''
            for byte in srcHex:
                decimal = int(byte, 16)
                ipAddress += (str(decimal) + '.')
            ipAddress = ipAddress[:-1]
            messageToPrint = 'You received: {} from: {}'.format(msgFromPkt, ipAddress)
        return messageToPrint
                
        
    def getHexStringFromPacket(self, packet):
        hexVals = [elem.encode("hex") for elem in packet]
        hexString = ''
        for i in range(0,19,2):
            hex1 = hexVals[i]
            hex2 = hexVals[i+1]
            twoByte = hex1 + hex2
            hexString += (twoByte + ' ')
        return hexString.strip()
        
    def ipCheckSum(self, header):
        '''
        calculates check sum for ip header
        '''
        hexSections = header.split()
        result = 0
        for piece in hexSections:
            intPiece = int(piece, 16)
            result += intPiece
            binResult = bin(result)
            if len(binResult) == 19:
                result -= 65536
                result += 1
        binResult = bin(result)[2:]
        while(len(binResult) < 16):
            binResult = ('0' + binResult)
        onesComp = ''
        for bit in binResult:
            if bit == '0':
                onesComp += '1'
            if bit == '1':
                onesComp += '0'
        finalByte = int(onesComp, 2)
        return finalByte
    
    def udpCheckSum(self, ipheader, udpheader, appheader):
        '''
        calculates check sum for udp header
        '''
        ipHexString = ipheader.split()
        udpHexString = udpheader.split()
        hexSections = [ipHexString[6], ipHexString[7], ipHexString[8], ipHexString[9], '0011', udpHexString[2]]
        for p in udpHexString:
            hexSections.append(p)
        l = appheader.encode("hex")
        msgChunks = [l[i:i + 4] for i in range(0, len(l), 4)]
        for msg in msgChunks:
            hexSections.append(msg)
        if len(hexSections[len(hexSections)-1]) == 2: #add pad byte
            hexSections[len(hexSections)-1] += '00'
        result = 0
        for piece in hexSections:
            intPiece = int(piece, 16)
            result += intPiece
            binResult = bin(result)
            if len(binResult) == 19:
                result -= 65536
                result += 1
        binResult = bin(result)[2:]
        while(len(binResult) < 16):
            binResult = ('0' + binResult)
        onesComp = ''
        for bit in binResult:
            if bit == '0':
                onesComp += '1'
            if bit == '1':
                onesComp += '0'
        finalByte = int(onesComp, 2)
        return finalByte
    
    def calcUDPLength(self, appdata):
        hexString = appdata.encode("hex")
        hexStringLen = len(hexString)
        nbrOfBytes = hexStringLen/2
        return 8 + nbrOfBytes
    
    def calcIPLength(self, appdata):
        hexString = appdata.encode("hex")
        hexStringLen = len(hexString)
        nbrOfBytes = hexStringLen/2
        return 28+nbrOfBytes
    
    def packHeadersIntoBitString(self, ipheader, transheader, appheader):
        '''
        packs headers into a bit string which will be our packet datq.
        '''
        ipformatString = '!BBHHHBBHLL'
        tranformatString = '!HHHH'
        messageLength = str(len(appheader.message))
        appformatString = '<{}s'.format(messageLength)
        packet = struct.pack(ipformatString, ipheader.versionAndIhl, ipheader.tos, ipheader.totalLength, ipheader.fragmentID, ipheader.fragmentOffset,
                           ipheader.ttl, ipheader.protocol, ipheader.checkSum, int(ipheader.sourceAddress,16), int(ipheader.destAddress,16))
        packet += struct.pack(tranformatString, transheader.sourcePort, transheader.destinationPort, transheader.length, transheader.checkSum)
        packet += struct.pack(appformatString, appheader.message)
        return packet
            
if __name__ == '__main__':
    Client()
        
    