'''
Builds a Transport Header

@author: James Engelsma and Joshua Engelsma
@version: 1.0
'''

class TransHeader(object):
    
    def __init__(self, port=0x0DA3):
        self.sourcePort = port
        self.destinationPort = port
        self.length = 8
        self.checkSum = 0
        self.hexString = '0da3 0da3 {} {}'.format(str(hex(self.length))[2:], str(hex(self.checkSum))[2:])
        
    def refreshHexString(self):
        self.hexString = '0da3 0da3 {} {}'.format(str(hex(self.length))[2:], str(hex(self.checkSum))[2:])